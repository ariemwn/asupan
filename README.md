# ASUPAN
**A**utomated **S**cripting **U**tilities for **P**ractical **A**nd **N**on-production

### Tentang ASUPAN
ASUPAN adalah repositori yang berisi skrip-skrip praktis yang dapat langsung digunakan untuk tahap deployment berbagai aplikasi. Skrip-skrip ini dirancang untuk memudahkan proses deployment, tetapi tidak disarankan untuk digunakan pada server produksi tanpa pengujian dan validasi terlebih dahulu. Skrip-skrip ini bertujuan untuk meriahkan status LinkedIn [Arie.Ibra](https://www.linkedin.com/in/arieibra/) dan diambil atau dipull dari berbagai sumber. Selamat bersenang-senang!

### Peringatan
**Penting:** Skrip yang ada di sini sebaiknya tidak digunakan pada server produksi. Gunakan skrip ini dengan risiko Anda sendiri karena lingkungan yang berbeda mungkin memerlukan penyesuaian. Jika Anda sudah yakin skrip aman untuk deployment, silakan gunakan.