# Docker Container Inspector

Sebuah script sederhana untuk menampilkan informasi container Docker yang sedang berjalan, termasuk nama, status, port, dan jaringan dalam bentuk tabel.

## Fitur

- Menginspeksi semua container Docker yang berjalan dan berhenti.
- Menampilkan informasi penting seperti nama container, status, port yang digunakan, dan jaringan yang terhubung.
- Menampilkan informasi dalam format tabel yang mudah dibaca.

## Prasyarat

Pastikan Anda sudah menginstal:

- Python 3.x
- Docker
- Library `docker` untuk Python
- Library `tabulate` untuk Python

Anda bisa menginstal library yang diperlukan dengan menjalankan:

```bash
pip3 install docker tabulate
```

## Cara Penggunaan

1. Clone repository ini ke mesin lokal Anda atau wget langsung skripnya

   ```bash
   git clone <repository_url>
   cd <repository_directory>
   ```

2. Pastikan Docker daemon berjalan di mesin Anda.

3. Jalankan script `network_docker.py`:

   ```bash
   python3 network_docker.py
   ```
4. Script akan menampilkan informasi container dalam bentuk tabel.

**Happy Coding!** 🚀