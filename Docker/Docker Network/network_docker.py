import docker
from tabulate import tabulate

# Inisialisasi klien Docker
client = docker.from_env()

# Fungsi untuk menampilkan informasi container
def get_container_info():
    # Ambil daftar container yang berjalan
    containers = client.containers.list(all=True)

    table = []

    for container in containers:
        # Inspect container
        container_info = client.api.inspect_container(container.id)
        
        # Ambil informasi yang diperlukan
        name = container_info['Name'].strip('/')
        status = container_info['State']['Status']
        
        # Ambil informasi port
        ports = container_info['NetworkSettings']['Ports']
        port_info = ', '.join([f"{container_port} -> {host_ports[0]['HostPort']}" for container_port, host_ports in ports.items() if host_ports])
        
        # Ambil informasi jaringan tambahan jika ada
        networks = container_info['NetworkSettings']['Networks']
        network_info = ', '.join([f"{net_name}: {net_info['IPAddress']}" for net_name, net_info in networks.items()])

        # Tambahkan informasi container ke tabel
        table.append([name, status, port_info, network_info])

    # Tampilkan informasi container dalam bentuk tabel
    headers = ["Name", "Status", "Ports", "Networks"]
    print(tabulate(table, headers, tablefmt="pretty"))

if __name__ == "__main__":
    get_container_info()
