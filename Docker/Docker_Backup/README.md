[TOC]

### Cara Menggunakan Skrip Backup dan Restore Docker Image

Skrip Python ini digunakan untuk mem-backup dan me-restore Docker image dari host atau dari container Docker-in-Docker (DinD) environment. Skrip ini juga mendukung backup dan restore melalui SSH/rsync. Berikut adalah langkah-langkah untuk menggunakan skrip ini.

#### Persyaratan
1. Python 3.x
2. Docker
3. Rsync (untuk fitur SSH/rsync)

#### Instalasi
1. Clone repositori ini atau salin skrip ke direktori lokal Anda.
2. Pastikan Anda memiliki akses ke Docker dan rsync.

#### Menjalankan Skrip

1. Buka terminal.
2. Jalankan skrip dengan perintah:
    ```sh
    python3 nama_skrip.py
    ```

### Menu Opsi

Ketika skrip dijalankan, Anda akan melihat menu berikut:

```
========================================
Pilih opsi:
1. Daftar container yang aktif
2. Daftar backup image yang tersedia
3. Backup Docker images dari host
4. Backup Docker images dari container master DinD
5. Restore Docker images
6. Restore Docker images ke banyak target DinD
7. Restore Docker images via SSH/rsync
8. Backup Docker images via SSH/rsync
9. Keluar
========================================
```

#### Penjelasan Menu

1. **Daftar container yang aktif**: Menampilkan daftar semua container yang sedang berjalan di host.
2. **Daftar backup image yang tersedia**: Menampilkan daftar semua file backup image yang tersedia di direktori backup.
3. **Backup Docker images dari host**: Melakukan backup semua Docker image dari host ke direktori backup.
4. **Backup Docker images dari container master DinD**: Melakukan backup semua Docker image dari container master DinD ke direktori backup.
5. **Restore Docker images**: Restore Docker image dari direktori backup ke host atau container DinD.
6. **Restore Docker images ke banyak target DinD**: Restore Docker image dari direktori backup ke beberapa container DinD.
7. **Restore Docker images via SSH/rsync**: Mengirim file backup ke server remote dan restore di sana.
8. **Backup Docker images via SSH/rsync**: Backup Docker image dari host dan mengirim file backup ke server remote.
9. **Keluar**: Keluar dari program.

### Contoh Penggunaan

1. **Backup Docker images dari host**:
   - Pilih opsi 3
   - Skrip akan membuat direktori backup jika belum ada, dan mem-backup semua Docker image dari host ke direktori backup.

2. **Restore Docker images ke host**:
   - Pilih opsi 5
   - Masukkan `host` sebagai lokasi restore.
   - Masukkan nama file image yang ingin di-restore atau `all` untuk me-restore semua image.

3. **Backup Docker images via SSH/rsync**:
   - Pilih opsi 8
   - Masukkan alamat server target (user@host).
   - Masukkan direktori tujuan di server target (/home/USER).

### Catatan Tambahan

- Skrip akan keluar secara otomatis jika input yang diberikan kosong atau tidak valid.
- Pastikan Docker daemon berjalan dan Anda memiliki izin yang sesuai untuk menjalankan perintah Docker.
- Jika menggunakan opsi SSH/rsync, pastikan Anda memiliki akses SSH ke server remote dan rsync terinstal.

Selamat menggunakan skrip ini untuk mempermudah pengelolaan Docker images Anda!