import os
import subprocess
import sys
from pathlib import Path

# Warna terminal
GREEN = "\033[0;32m"
YELLOW = "\033[1;33m"
RED = "\033[0;31m"
NC = "\033[0m"  # No Color

# Direktori penyimpanan backup
BACKUP_DIR = Path.home() / "Downloads" / "image_backup_docker"
TEMP_DIR = Path("/tmp/docker_restore_temp")

def create_directory(path):
    if not path.exists():
        print(f"{YELLOW}Membuat direktori: {path}{NC}")
        path.mkdir(parents=True, exist_ok=True)
    else:
        print(f"{GREEN}Direktori sudah ada: {path}{NC}")

def list_active_containers():
    print(f"{YELLOW}Daftar container yang aktif:{NC}")
    subprocess.run(["docker", "ps", "--format", "table {{.Names}}\t{{.Image}}"])

def list_backup_images():
    print(f"{YELLOW}Daftar backup image yang tersedia di {BACKUP_DIR}:{NC}")
    backup_files = list(BACKUP_DIR.glob("*.tar"))
    if backup_files:
        for file in backup_files:
            print(file.name)
    else:
        print(f"{RED}Tidak ada backup image yang tersedia.{NC}")

def list_target_images(restore_location, master_container=None):
    if restore_location == "host":
        subprocess.run(["docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], stdout=open(TEMP_DIR / "target_images.txt", "w"))
        print(f"{YELLOW}Daftar image yang ada di host:{NC}")
        subprocess.run(["cat", TEMP_DIR / "target_images.txt"])
    elif restore_location == "dind" and master_container:
        subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], stdout=open(TEMP_DIR / f"{master_container}_images.txt", "w"))
        print(f"{YELLOW}Daftar image yang ada di DinD {master_container}:{NC}")
        subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "table {{.Repository}}:{{.Tag}}"])

def backup_docker_images(restore_location, master_container=None):
    if restore_location == "host":
        images = subprocess.run(["docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], capture_output=True, text=True).stdout.splitlines()
    elif restore_location == "dind" and master_container:
        images = subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], capture_output=True, text=True).stdout.splitlines()
    else:
        print(f"{RED}Lokasi restore tidak valid.{NC}")
        return

    for image_name in images:
        if "<none>" in image_name:
            image_name = image_name.replace("<none>", "latest")
        image_file = BACKUP_DIR / f"{image_name.replace(':', '_').replace('/', '_')}.tar"
        if image_file.exists():
            print(f"{GREEN}File backup image sudah ada: {image_file}{NC}")
        else:
            print(f"{YELLOW}Menyimpan Docker image {image_name} ke file: {image_file}{NC}")
            if restore_location == "host":
                subprocess.run(["docker", "save", "-o", image_file, image_name])
            elif restore_location == "dind" and master_container:
                subprocess.run(["docker", "exec", master_container, "docker", "save", "-o", f"/tmp/{image_file.name}", image_name])
                subprocess.run(["docker", "cp", f"{master_container}:/tmp/{image_file.name}", image_file])
                subprocess.run(["docker", "exec", master_container, "rm", f"/tmp/{image_file.name}"])

def sanitize_image_name(image_name):
    sanitized_image = image_name.replace('.tar', '').replace('_', ':', 1).replace('_', '-', 1)
    return sanitized_image

def image_exists(image, restore_location, master_container=None):
    sanitized_image = sanitize_image_name(image)
    if restore_location == "host":
        result = subprocess.run(["docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], capture_output=True, text=True)
    elif restore_location == "dind" and master_container:
        result = subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], capture_output=True, text=True)
    else:
        return False
    return sanitized_image in result.stdout

def get_input(prompt):
    value = input(prompt).strip()
    if not value:
        print(f"{RED}Input tidak boleh kosong!{NC}")
        sys.exit(1)
    return value

def restore_docker_images():
    restore_location = get_input("Pilih lokasi restore (host atau dind): ")
    master_container = None
    if restore_location == "dind":
        list_active_containers()
        master_container = get_input("Masukkan nama container master DinD: ")
        if master_container not in subprocess.run(["docker", "ps", "--format", "{{.Names}}"], capture_output=True, text=True).stdout.splitlines():
            print(f"{RED}Container DinD dengan nama {master_container} tidak aktif.{NC}")
            sys.exit(1)

    create_directory(TEMP_DIR)
    list_target_images(restore_location, master_container)
    list_backup_images()
    image_file = get_input("Masukkan nama image file yang ingin di-restore (contoh: nginx_image.tar, atau 'all' untuk semua): ")

    if image_file == "all":
        for image_path in BACKUP_DIR.glob("*.tar"):
            image_name = sanitize_image_name(image_path.name)
            if image_exists(image_name, restore_location, master_container):
                print(f"{GREEN}Image {image_name} sudah ada, tidak perlu di-restore.{NC}")
            else:
                print(f"{YELLOW}Mengimpor Docker image dari file: {image_path}{NC}")
                if restore_location == "host":
                    subprocess.run(["docker", "load", "-i", image_path])
                elif restore_location == "dind" and master_container:
                    subprocess.run(["docker", "cp", image_path, f"{master_container}:/tmp/{image_path.name}"])
                    subprocess.run(["docker", "exec", master_container, "docker", "load", "-i", f"/tmp/{image_path.name}"])
                    subprocess.run(["docker", "exec", master_container, "rm", f"/tmp/{image_path.name}"])
    elif (BACKUP_DIR / image_file).exists():
        image_name = sanitize_image_name(image_file)
        if image_exists(image_name, restore_location, master_container):
            print(f"{GREEN}Image {image_name} sudah ada, tidak perlu di-restore.{NC}")
        else:
            print(f"{YELLOW}Mengimpor Docker image dari file: {BACKUP_DIR / image_file}{NC}")
            if restore_location == "host":
                subprocess.run(["docker", "load", "-i", BACKUP_DIR / image_file])
            elif restore_location == "dind" and master_container:
                subprocess.run(["docker", "cp", BACKUP_DIR / image_file, f"{master_container}:/tmp/{image_file}"])
                subprocess.run(["docker", "exec", master_container, "docker", "load", "-i", f"/tmp/{image_file}"])
                subprocess.run(["docker", "exec", master_container, "rm", f"/tmp/{image_file}"])
    else:
        print(f"{RED}File backup image tidak ditemukan: {BACKUP_DIR / image_file}{NC}")
        sys.exit(1)

    TEMP_DIR.rmdir()

def restore_docker_images_to_multiple_dind(containers=None, image_file=None):
    if containers is None:
        containers = get_input("Masukkan nama container DinD yang akan di-restore, dipisahkan dengan koma: ")
    container_list = containers.split(',')

    create_directory(TEMP_DIR)
    
    for master_container in container_list:
        if master_container not in subprocess.run(["docker", "ps", "--format", "{{.Names}}"], capture_output=True, text=True).stdout.splitlines():
            print(f"{RED}Container DinD dengan nama {master_container} tidak aktif.{NC}")
            continue

        subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], stdout=open(TEMP_DIR / f"{master_container}_images.txt", "w"))
        print(f"{YELLOW}Daftar image yang ada di DinD {master_container}:{NC}")
        subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "table {{.Repository}}:{{.Tag}}"])

    list_backup_images()

    if image_file is None:
        image_file = get_input("Masukkan nama image file yang ingin di-restore (contoh: nginx_image.tar, atau 'all' untuk semua): ")

    for master_container in container_list:
        if image_file == "all":
            for image_path in BACKUP_DIR.glob("*.tar"):
                image_name = sanitize_image_name(image_path.name)
                if image_name in open(TEMP_DIR / f"{master_container}_images.txt").read():
                    print(f"{GREEN}Image {image_name} sudah ada di container {master_container}, tidak perlu di-restore.{NC}")
                else:
                    print(f"{YELLOW}Mengimpor Docker image dari file: {image_path} ke DinD {master_container}{NC}")
                    subprocess.run(["docker", "cp", image_path, f"{master_container}:/tmp/{image_path.name}"])
                    subprocess.run(["docker", "exec", master_container, "docker", "load", "-i", f"/tmp/{image_path.name}"])
                    subprocess.run(["docker", "exec", master_container, "rm", f"/tmp/{image_path.name}"])
                    subprocess.run(["docker", "exec", master_container, "docker", "images", "--format", "{{.Repository}}:{{.Tag}}"], stdout=open(TEMP_DIR / f"{master_container}_images.txt", "w"))
        elif (BACKUP_DIR / image_file).exists():
            image_name = sanitize_image_name(image_file)
            if image_name in open(TEMP_DIR / f"{master_container}_images.txt").read():
                print(f"{GREEN}Image {image_name} sudah ada di container {master_container}, tidak perlu di-restore.{NC}")
            else:
                print(f"{YELLOW}Mengimpor Docker image dari file: {BACKUP_DIR / image_file} ke DinD {master_container}{NC}")
                subprocess.run(["docker", "cp", BACKUP_DIR / image_file, f"{master_container}:/tmp/{image_file}"])
                subprocess.run(["docker", "exec", master_container, "docker", "load", "-i", f"/tmp/{image_file}"])
                subprocess.run(["docker", "exec", master_container, "rm", f"/tmp/{image_file}"])
        else:
            print(f"{RED}File backup image tidak ditemukan: {BACKUP_DIR / image_file}{NC}")

    TEMP_DIR.rmdir()

def restore_docker_images_via_ssh():
    target_server = get_input("Masukkan alamat server target (user@host): ")
    target_dir = get_input("Masukkan direktori tujuan di server target (/home/USER): ")

    print(f"{YELLOW}Mengirim file backup ke server target...{NC}")
    subprocess.run(["rsync", "-avz", BACKUP_DIR, f"{target_server}:{target_dir}"])

    print(f"{YELLOW}Mengimpor Docker image di server target...{NC}")
    subprocess.run(["ssh", target_server, f"for file in {target_dir}/*.tar; do docker load -i $file; done"])

    print(f"{GREEN}Proses restore via SSH/rsync selesai.{NC}")

def backup_docker_images_via_ssh():
    target_server = get_input("Masukkan alamat server target (user@host): ")
    target_dir = get_input("Masukkan direktori tujuan di server target (/home/USER): ")

    create_directory(BACKUP_DIR)
    backup_docker_images("host")

    print(f"{YELLOW}Mengirim file backup ke server target...{NC}")
    subprocess.run(["rsync", "-avz", BACKUP_DIR, f"{target_server}:{target_dir}"])

    print(f"{GREEN}Proses backup via SSH/rsync selesai.{NC}")

def menu():
    print(f"{YELLOW}========================================")
    print("Pilih opsi:")
    print("1. Daftar container yang aktif")
    print("2. Daftar backup image yang tersedia")
    print("3. Backup Docker images dari host")
    print("4. Backup Docker images dari container master DinD")
    print("5. Restore Docker images")
    print("6. Restore Docker images ke banyak target DinD")
    print("7. Restore Docker images via SSH/rsync")
    print("8. Backup Docker images via SSH/rsync")
    print("9. Keluar")
    print(f"========================================{NC}")

    choice = get_input("Masukkan pilihan Anda [1-9]: ")
    if choice == "1":
        list_active_containers()
    elif choice == "2":
        list_backup_images()
    elif choice == "3":
        create_directory(BACKUP_DIR)
        backup_docker_images("host")
    elif choice == "4":
        create_directory(BACKUP_DIR)
        master_container = get_input("Masukkan nama container master: ")
        backup_docker_images("dind", master_container)
    elif choice == "5":
        restore_docker_images()
    elif choice == "6":
        restore_docker_images_to_multiple_dind()
    elif choice == "7":
        restore_docker_images_via_ssh()
    elif choice == "8":
        backup_docker_images_via_ssh()
    elif choice == "9":
        print(f"{GREEN}Keluar...{NC}")
        sys.exit(0)
    else:
        print(f"{RED}Pilihan tidak valid!{NC}")
        sys.exit(1)

if __name__ == "__main__":
    while True:
        menu()
