#!/bin/bash

# Menambahkan DNS Google ke resolv.conf
echo "Menambahkan DNS Google..."
if ! grep -q "nameserver 8.8.8.8" /etc/resolv.conf; then
    echo "nameserver 8.8.8.8" | sudo tee -a /etc/resolv.conf
fi

if ! grep -q "nameserver 8.8.4.4" /etc/resolv.conf; then
    echo "nameserver 8.8.4.4" | sudo tee -a /etc/resolv.conf
fi

echo "DNS Google telah ditambahkan."

# Membuat atau mengubah konfigurasi Docker daemon
DOCKER_CONFIG_FILE="/etc/docker/daemon.json"

echo "Mengatur ulang konfigurasi Docker..."
if [ ! -f "$DOCKER_CONFIG_FILE" ]; then
    sudo touch $DOCKER_CONFIG_FILE
fi

sudo tee $DOCKER_CONFIG_FILE > /dev/null <<EOF
{
  "dns": ["8.8.8.8", "8.8.4.4"],
  "registry-mirrors": ["https://mirror.gcr.io"]
}
EOF

echo "Konfigurasi Docker telah diatur."

# Restart Docker
echo "Merestart Docker..."
sudo systemctl restart docker

# Jika Docker tidak bisa direstart, berikan pesan kesalahan dan keluar
if [ $? -ne 0 ]; then
    echo "Docker gagal direstart. Silakan periksa status layanan Docker dengan 'systemctl status docker.service' dan 'journalctl -xeu docker.service'."
    exit 1
fi

echo "Docker telah direstart."

# Memeriksa koneksi ke registry Docker
echo "Memeriksa koneksi ke registry Docker..."
curl -v https://mirror.gcr.io/v2/ || { echo "Koneksi ke mirror Docker GCR gagal."; exit 1; }

# Verifikasi apakah perubahan berhasil
echo "Memverifikasi konfigurasi Docker..."
docker info | grep -i "registry"
docker info | grep -i "dns"

echo "Skrip selesai. Silakan coba kembali menjalankan perintah docker-compose up -d."
