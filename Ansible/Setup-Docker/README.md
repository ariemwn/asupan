# Ansible Playbook: Setup Docker and Monitoring Tools

Playbook ini digunakan untuk mengatur Docker, Docker Compose, Go, dan Node Exporter pada sistem lokal (localhost). Playbook ini berguna untuk mengotomatiskan proses instalasi dan konfigurasi alat-alat ini, serta memastikan semuanya berjalan dengan baik.

## Fitur
- Instalasi Docker dan Docker Compose
- Penambahan user ke grup Docker
- Instalasi Go dan pengaturan PATH
- Instalasi dan konfigurasi Node Exporter untuk pemantauan
- Setup cAdvisor menggunakan Docker Compose

## Persyaratan
- Ansible
- jq

## Cara Menjalankan Ansible Playbook dan Pembatasan untuk Localhost


1. **Instalasi Ansible dan Dependensi Lainnya**

   Jalankan perintah berikut untuk memperbarui sistem Anda, menginstal Ansible, dan dependensi yang diperlukan:

   ```bash
   sudo apt update && sudo apt upgrade -y
   sudo apt install ansible -y
   sudo apt install -y jq
   ```

2. **Unduh YAML Playbook**

   Pastikan Anda sudah memiliki file playbook `setup_docker_localhost.yml`. Jika belum, Anda bisa mengunduhnya.

3. **Jalankan Ansible Playbook**

   Gunakan perintah berikut untuk menjalankan playbook di localhost:

   ```bash
   ansible-playbook -i "localhost," -c local setup_docker_localhost.yml --ask-become-pass
   ```

## Cara Memverifikasi Hasil Setup

Setelah playbook selesai dijalankan, ada beberapa langkah yang bisa dilakukan untuk memverifikasi bahwa Docker dan alat-alat lainnya telah terinstal dan dikonfigurasi dengan benar.

1. **Periksa Versi Docker**

   Pastikan Docker telah terinstal dengan benar dengan memeriksa versinya:

   ```bash
   docker --version
   ```

   Hasil yang diharapkan:
   ```plaintext
   Docker version 20.10.7, build f0df350
   ```

2. **Periksa Versi Docker Compose**

   Pastikan Docker Compose telah terinstal dengan benar dengan memeriksa versinya:

   ```bash
   docker-compose --version
   ```

   Hasil yang diharapkan:
   ```plaintext
   docker-compose version 1.29.2, build 5becea4c
   ```

3. **Periksa Versi Go**

   Pastikan Go telah terinstal dengan benar dengan memeriksa versinya:

   ```bash
   go version
   ```

   Hasil yang diharapkan:
   ```plaintext
   go version go1.20.2 linux/amd64
   ```

4. **Verifikasi Node Exporter**

   Pastikan Node Exporter berjalan dengan benar:

   ```bash
   systemctl status node_exporter
   ```

   Hasil yang diharapkan:
   ```plaintext
   ● node_exporter.service - Node Exporter
      Loaded: loaded (/etc/systemd/system/node_exporter.service; enabled; vendor preset: enabled)
      Active: active (running) since [timestamp]
      ...
   ```

5. **Cek cAdvisor**

   Jalankan cAdvisor menggunakan Docker Compose dan verifikasi bahwa cAdvisor berjalan dengan benar:

   ```bash
   cd ~/monitoring
   docker-compose -f docker-compose-cadvisor.yml up -d
   ```

   Periksa apakah cAdvisor berjalan dengan memeriksa kontainer Docker:

   ```bash
   docker ps
   ```

   Hasil yang diharapkan:
   ```plaintext
   CONTAINER ID   IMAGE                COMMAND           CREATED          STATUS          PORTS                    NAMES
   [container_id] gcr.io/cadvisor/cadvisor "/usr/bin/cadvisor"   [timestamp]    Up [time]        0.0.0.0:8080->8080/tcp   monitoring_cadvisor_1
   ```