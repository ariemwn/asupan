# SEO Audit Script

Skrip ini digunakan untuk melakukan audit SEO pada halaman web tertentu. Skrip akan memeriksa berbagai elemen penting untuk SEO seperti tag title, meta description, tag H1 dan H2, link kanonis, teks alt pada gambar, keberadaan robots.txt, penggunaan SSL, dan link yang rusak.

## Fitur
- Mengecek keberadaan dan panjang tag title
- Mengecek keberadaan dan panjang meta description
- Mengecek keberadaan dan panjang tag H1 dan H2
- Mengecek link kanonis
- Mengecek gambar yang tidak memiliki teks alt
- Mengecek keberadaan robots.txt
- Mengecek apakah halaman menggunakan SSL (HTTPS)
- Mengecek link yang rusak (broken links)

## Instalasi
1. Clone repository ini atau download langsung saja:
    ```bash
    git clone https://gitlab.com/ariemwn/asupan/ 
    ```
2. Pindah ke direktori repository:
    ```bash
    cd asupan/SEO\ SIP
    ```
3. Install dependencies yang dibutuhkan:
    ```bash
    pip install -r requirements.txt
    ```

## Penggunaan
1. Jalankan skrip dan masukkan URL yang ingin diaudit:
    ```bash
    python3 auditseo.py
    ```
2. Masukkan URL saat diminta:
    ```bash
    Enter the URL to audit: https://example.com
    ```

## Contoh Output
```bash
Title: Example Title
Title Length: 13 characters
Meta Description: Example meta description.
Meta Description Length: 28 characters
H1-1: Example H1
H1-1 Length: 9 characters
...
```

## Catatan
- Pastikan URL yang dimasukkan valid dan dapat diakses.
- Skrip ini hanya memeriksa link dalam domain yang sama untuk menghindari crawling seluruh web.